import java.util.Scanner;

public class MultiplyTable {

	public static void main(String[] args) {
	
		try (Scanner input = new Scanner(System.in)) {
			//row
			System.out.println("Enter the row and column:");
			int height = Integer.parseInt(input.next());
			

			//column
			int length = Integer.parseInt(input.next());
			

			for (int i=1; i<=height; i++) 
			{
			    for (int j = 1; j <= length; j++ )
			    {
			    	
			        System.out.print(i * j);
			        
			        //Add a tab after each number to form every column.
			        System.out.print("\t"); 
			        }   

			    //Add a break line for each row.
			    System.out.println();
			}
		}

	}

}